'use strict';

/**
 * Define stylelint.
 */
var combo  = require('./combo'),
    themes = require('../tools/files-router').get('themes'),
    _      = require('underscore');

var themeLessFiles = [];

_.each(themes, function (theme, name) {
    themeLessFiles.push('<%= combo.autopath(\''+name+'\', path.pub) %>/**/*.less');
});
    themeLessFiles.push('<%= path.less.frontend %>/**/*.less');

module.exports = {
    stylelint: {
        options: {
            configFile: '.stylelintrc',
            formatter: 'string',
            ignoreDisables: false,
            failOnError: true,
            outputFile: '',
            reportNeedlessDisables: false,
            fix: false,
            syntax: ''
        },
        src: [
            themeLessFiles
        ],
    }
};
