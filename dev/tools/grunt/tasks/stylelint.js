/**
 * Define task stylelint.
 */

module.exports = function (grunt) {
    'use strict';

    var stylelintConfig = require('../configs/stylelint');
    var target = grunt.option('only-once-less');

    if(typeof target !== "undefined") {
        stylelintConfig.stylelint.src.pop();
        stylelintConfig.stylelint.src.push(target);
    }

    grunt.initConfig(stylelintConfig);
    grunt.loadNpmTasks( 'grunt-stylelint' );
};
